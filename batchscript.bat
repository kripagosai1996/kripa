@echo off

setlocal EnableDelayedExpansion

(
  
  For /F "usebackq tokens=1-3  delims=" %%a in (NPT.csv) Do (
    set "line=%%a"
    setlocal EnableDelayedExpansion
    set "line="!line:,=","!""
    For /F "tokens=1-3  delims=," %%a in ("!line!") Do (
        echo %%~a, %%~b, %%~c 
    )
  )
) > output.csv